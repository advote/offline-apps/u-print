/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;
import org.springframework.stereotype.Component;

/**
 * Exposes the state of the application via JavaFx "observable properties" system.
 *
 * <p>
 * Allows to bind GUI elements to some of the application business-oriented information values.
 * </p>
 *
 * <h3 id="predefined">List of properties</h3>
 *
 * <table class="striped" style="text-align:left">
 * <thead>
 * <tr>
 * <th scope="col">Property</th>
 * <th scope="col">Description</th>
 * <th scope="col">Initial value</th>
 * </tr>
 * </thead>
 * <tbody>
 * <tr>
 * <th scope="row">controlComponentsPublicKeysImported</th>
 * <td>Whether the control component public keys have been imported</td>
 * <td>false</td>
 * </tr>
 * <tr>
 * <th scope="row">printerFileGenerationInProgress</th>
 * <td>Whether a printer file is being generated</td>
 * <td>false</td>
 * </tr>
 * <tr>
 * <th scope="row">printerFileImportInProgress</th>
 * <td>Whether a printer file is being imported for visualization</td>
 * <td>false</td>
 * </tr>
 * <tr>
 * <th scope="row">printerKeyGenerationInProgress</th>
 * <td>Whether a printer key is being generated</td>
 * <td>false</td>
 * </tr>
 * </tbody>
 * </table>
 */
@Component
public class ApplicationState {

  private final BooleanProperty controlComponentsPublicKeysImported;
  private final BooleanProperty printerFileGenerationInProgress;
  private final BooleanProperty printerKeyGenerationInProgress;
  private final BooleanProperty printerFileImportInProgress;

  /**
   * Create a new application state with the initial values.
   */
  public ApplicationState() {
    controlComponentsPublicKeysImported = new SimpleBooleanProperty(this, "controlComponentsPublicKeysImported", false);
    printerFileGenerationInProgress = new SimpleBooleanProperty(this, "printerFileGenerationInProgress", false);
    printerKeyGenerationInProgress = new SimpleBooleanProperty(this, "printerKeyGenerationInProgress", false);
    printerFileImportInProgress = new SimpleBooleanProperty(this, "printerFileImportInProgress", false);
  }

  /**
   * Set whether the control component public keys have been imported. Setting this property to true will make the
   * {@code GeneratePrinterFile} view accessible by enabling its side menu button, setting it to false will disable the
   * side menu button (see {@link RootLayoutController}).
   *
   * @param controlComponentsPublicKeysImported whether the control component public keys have been imported.
   */
  public void setControlComponentsPublicKeysImported(boolean controlComponentsPublicKeysImported) {
    this.controlComponentsPublicKeysImported.set(controlComponentsPublicKeysImported);
  }

  /**
   * Whether the control component public keys have been imported.
   *
   * @return a boolean {@link javafx.beans.property.ReadOnlyProperty} that describes whether the control component
   * public key has been imported.
   */
  public ReadOnlyBooleanProperty controlComponentsPublicKeysImportedProperty() {
    return controlComponentsPublicKeysImported;
  }

  /**
   * Set whether a printer file is being generated. Setting this property to true will make the application's {@link
   * #isLoading()} state to be true.
   *
   * @param printerFileGenerationInProgress whether a printer file is being generated
   */
  public void setPrinterFileGenerationInProgress(boolean printerFileGenerationInProgress) {
    this.printerFileGenerationInProgress.set(printerFileGenerationInProgress);
  }

  /**
   * Whether a printer file is being imported for visualization.
   *
   * @return a boolean {@link javafx.beans.property.ReadOnlyProperty} that describes whether a printer file is being
   * imported for visualization.
   */
  public ReadOnlyBooleanProperty printerFileGenerationInProgressProperty() {
    return this.printerFileGenerationInProgress;
  }

  /**
   * Set whether a printer file is being imported for visualization. Setting this property to true will make the
   * application's {@link #isLoading()} state to be true.
   *
   * @param printerFileImportInProgress whether a printer file is being imported for visualization.
   */
  public void setPrinterFileImportInProgress(boolean printerFileImportInProgress) {
    this.printerFileImportInProgress.set(printerFileImportInProgress);
  }

  /**
   * Whether a printer file is being imported for visualization.
   *
   * @return a boolean {@link javafx.beans.property.ReadOnlyProperty} that describes whether a printer file is being
   * imported for visualization.
   */
  public ReadOnlyBooleanProperty printerFileImportInProgressProperty() {
    return this.printerFileImportInProgress;
  }

  /**
   * Set whether a printer key is being generated. Setting this property to true will make the application's {@link
   * #isLoading()} state to be true, a spinner will appear and the content will be disabled until this property is set
   * back to false.
   *
   * @param printerKeyGenerationInProgress whether a printer key is being generated
   */
  public void setPrinterKeyGenerationInProgress(boolean printerKeyGenerationInProgress) {
    this.printerKeyGenerationInProgress.set(printerKeyGenerationInProgress);
  }

  /**
   * Whether a printer key is being generated.
   *
   * @return a boolean {@link javafx.beans.property.ReadOnlyProperty} that describes whether a printer key is being
   * generated.
   */
  public ReadOnlyBooleanProperty printerKeyGenerationInProgressProperty() {
    return printerKeyGenerationInProgress;
  }

  /**
   * Whether the application is busy with a background task at the moment. While the value returned by this method is
   * true the submenu and the language picker of the application will be disabled.
   *
   * @return a boolean {@link javafx.beans.value.ObservableValue} that describes whether the application is busy with a
   * background task at the moment
   *
   * @see #setPrinterFileGenerationInProgress(boolean)
   * @see #setPrinterFileImportInProgress(boolean)
   * @see #setPrinterKeyGenerationInProgress(boolean)
   */
  public ObservableBooleanValue isLoading() {
    return printerFileGenerationInProgress.or(printerFileImportInProgress)
                                          .or(printerKeyGenerationInProgress)
                                          .or(printerKeyGenerationInProgress);
  }
}
