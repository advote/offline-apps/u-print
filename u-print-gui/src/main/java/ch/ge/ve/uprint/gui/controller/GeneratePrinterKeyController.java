/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller;

import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.javafx.gui.control.MessageContainer;
import ch.ge.ve.javafx.gui.control.PrivateKeyInputGroup;
import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import ch.ge.ve.javafx.gui.utils.TaskUtils;
import ch.ge.ve.uprint.business.key.PrinterKeyGenerator;
import ch.ge.ve.uprint.gui.controller.exception.TaskExecutionException;
import com.google.common.base.Stopwatch;
import java.time.Duration;
import java.util.concurrent.ExecutionException;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

/**
 * JavaFX Controller for the generation of the printer's key.
 */
@Component
public class GeneratePrinterKeyController {
  private static final String ERROR_TRANSLATION_KEY   = "generate-printer-key.error-message";
  private static final String SUCCESS_TRANSLATION_KEY = "generate-printer-key.success-message";

  private static final Logger logger = LoggerFactory.getLogger(GeneratePrinterKeyController.class);

  private final ApplicationState    applicationState;
  private final PrinterKeyGenerator keyPairGenerator;
  private final TaskExecutor        taskExecutor;

  @FXML
  private MessageContainer successMessageContainer;

  @FXML
  private PrivateKeyInputGroup privateKeyInput;

  @FXML
  private HBox actionBox;

  private final ObjectProperty<FileBrowserService> fileBrowserService = new SimpleObjectProperty<>();

  @Autowired
  public GeneratePrinterKeyController(ApplicationState applicationState,
                                      FileBrowserService fileBrowserService,
                                      PrinterKeyGenerator keyPairGenerator,
                                      TaskExecutor taskExecutor) {
    this.applicationState = applicationState;
    this.keyPairGenerator = keyPairGenerator;
    this.taskExecutor = taskExecutor;
    this.fileBrowserService.setValue(fileBrowserService);
  }

  public final FileBrowserService getFileBrowserService() {
    return fileBrowserService.get();
  }

  public final ReadOnlyObjectProperty<FileBrowserService> fileBrowserServiceProperty() {
    return fileBrowserService;
  }

  /**
   * Validate the private key input field and launches the generation of the the printer key's pair.
   *
   * @see PrivateKeyInputGroup#validate()
   */
  @FXML
  public void generatePrinterKey() {
    if (privateKeyInput.validate()) {

      applicationState.setPrinterKeyGenerationInProgress(true);

      doKeyGeneration();
    }
  }

  private void doKeyGeneration() {
    Task<Duration> task = new Task<>() {
      @Override
      protected Duration call() throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();

        keyPairGenerator.generateAndStoreKeyPair(
            privateKeyInput.getDestinationPath(), privateKeyInput.getPassphrase()
        );

        return stopwatch.stop().elapsed();
      }
    };

    task.setOnFailed(event -> {
      logger.error("Key generation task failed.", task.getException());
      onKeyGenerationFailed(task);
    });

    task.setOnSucceeded(event -> {
      try {
        onKeyGenerationSuccess();
        logger.info("Printer key generation took [{}] seconds", task.get().getSeconds());
      } catch (ExecutionException e) {
        logger.error("Cannot retrieve the result of the printer key generation task", e);
        onKeyGenerationFailed(task);
        throw new TaskExecutionException("Cannot retrieve the result of the printer key generation task", e);
      } catch (InterruptedException e) {
        logger.error("The printer key generation task was interrupted", e);
        onKeyGenerationFailed(task);
        Thread.currentThread().interrupt();
      }
    });

    taskExecutor.execute(task);
  }

  private <T> void onKeyGenerationFailed(Task<T> task) {
    privateKeyInput.setErrorMessage(TaskUtils.formatErrorMessage(ERROR_TRANSLATION_KEY, task));
    applicationState.setPrinterKeyGenerationInProgress(false);
  }

  private void onKeyGenerationSuccess() {
    successMessageContainer.setMessage(LanguageUtils.getCurrentResourceBundle().getString(SUCCESS_TRANSLATION_KEY));
    applicationState.setPrinterKeyGenerationInProgress(false);
    privateKeyInput.clear();
    privateKeyInput.setManaged(false);
    privateKeyInput.setVisible(false);
    actionBox.setManaged(false);
    actionBox.setVisible(false);
  }
}
