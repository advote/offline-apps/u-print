/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.validator;

import ch.ge.ve.uprint.business.context.factory.ControlComponentsPublicKeysFileFactory;
import ch.ge.ve.uprint.gui.Uprint;
import com.jfoenix.validation.base.ValidatorBase;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A validator that proof-reads the control components public keys file, displaying an error message in case of
 * failure.
 */
public class ControlComponentsPublicKeysFileValidator extends ValidatorBase {

  private static final Logger logger = LoggerFactory.getLogger(ControlComponentsPublicKeysFileValidator.class);

  private final StringProperty baseMessage;

  /**
   * Create an empty control component public keys file validator.
   */
  public ControlComponentsPublicKeysFileValidator() {
    this(null);
  }

  /**
   * Create a control component public keys file validator with the given default message.
   *
   * @param message the default message of this validator.
   */
  public ControlComponentsPublicKeysFileValidator(String message) {
    super(message);

    this.baseMessage = new SimpleStringProperty(this, "baseMessage");
  }

  @Override
  protected void eval() {
    if (srcControl.get() instanceof TextInputControl) {
      String text = ((TextInputControl) srcControl.get()).getText();
      readPublicKeysFile(Paths.get(text));
    }
  }

  private void readPublicKeysFile(Path path) {
    try {
      Uprint.getContext()
            .getBean(ControlComponentsPublicKeysFileFactory.class)
            .readControlComponentsPublicKeysFile(path);

      hasErrors.set(false);
    } catch (Exception e) {
      logger.warn("Unreadable control components public keys file", e);

      hasErrors.set(true);
      setMessage(MessageFormat.format(getBaseMessage(), path.getFileName()));
    }
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Get the base message pattern.
   *
   * @return the base message pattern.
   */
  public final String getBaseMessage() {
    return baseMessage.get();
  }

  /**
   * Set the base message pattern. If this property is set the {@link #messageProperty()} of this validator will be
   * overridden using this pattern, the pattern must accept only one string argument which corresponds to the filename
   * of the path in the text field this validator is attached to.
   *
   * @param value the base message pattern.
   *
   * @see MessageFormat
   */
  public final void setBaseMessage(String value) {
    baseMessage.set(value);
  }

  /**
   * The base message pattern property.
   *
   * @return the base message pattern property.
   */
  public final StringProperty baseMessageProperty() {
    return baseMessage;
  }

}
