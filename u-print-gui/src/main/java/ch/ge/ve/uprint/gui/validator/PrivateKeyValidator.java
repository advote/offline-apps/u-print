/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.validator;

import ch.ge.ve.uprint.business.context.factory.PrivateKeyFactory;
import ch.ge.ve.uprint.gui.Uprint;
import com.jfoenix.validation.base.ValidatorBase;
import java.nio.file.Paths;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A validator that proof-reads the files chosen as the private key, and display an error message if the reading fails.
 * This validator is intended to be used in the passphrase input field.
 */
public class PrivateKeyValidator extends ValidatorBase {
  private static final Logger logger = LoggerFactory.getLogger(PrivateKeyValidator.class);

  private final StringProperty privateKeyPath;

  /**
   * Create an empty private key validator.
   */
  public PrivateKeyValidator() {
    this(null);
  }

  /**
   * Create a private key validator validator with the given default message.
   *
   * @param message the default message of this validator.
   */
  public PrivateKeyValidator(String message) {
    super(message);

    this.privateKeyPath = new SimpleStringProperty(this, "privateKeyPath");
  }

  @Override
  protected void eval() {
    if (srcControl.get() instanceof TextInputControl) {
      String text = ((TextInputControl) srcControl.get()).getText();
      try {
        Uprint.getContext()
              .getBean(PrivateKeyFactory.class)
              .readPrivateKey(Paths.get(getPrivateKeyPath()), text.toCharArray());
        hasErrors.set(false);
      } catch (Exception e) {
        logger.warn("Unreadable private key", e);
        hasErrors.set(true);
      }
    }
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Get the current private key path.
   *
   * @return the current private key path
   */
  public final String getPrivateKeyPath() {
    return privateKeyPath.get();
  }

  /**
   * Set the private key path for validation.
   *
   * @param value the new private key path for validation.
   */
  public final void setPrivateKeyPath(String value) {
    privateKeyPath.set(value);
  }

  /**
   * The private key path property.
   *
   * @return the private key path.
   */
  public final StringProperty privateKeyPathProperty() {
    return privateKeyPath;
  }
}
