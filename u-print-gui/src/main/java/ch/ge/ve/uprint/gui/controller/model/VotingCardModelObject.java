/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.gui.controller.model;

import ch.ge.ve.interfaces.ech.eCH0010.v6.AddressInformationType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.PersonMailAddressInfoType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.NamedCodeType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VoteType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingCardDeliveryType;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingPersonType;
import ch.ge.ve.model.convert.impl.ech0159.model.ECH0159VoteInformation;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import ch.ge.ve.uprint.business.ech0228.ECH0228CodeDesignation;
import com.google.common.base.Strings;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A {@link VotingCardDeliveryType.VotingCard} value object. New instances of this value object can only be created
 * with the provided factory method: {@link VotingCardModelObject#get(VotingCardDeliveryType.VotingCard, List)}.
 */
public class VotingCardModelObject {
  private final String                                 confirmationCode;
  private final String                                 finalizationCode;
  private final PersonMailAddressInfoType              voter;
  private final String                                 address;
  private final String                                 identificationCode;
  private final String                                 domainOfInfluence;
  private final String                                 sequenceNumber;
  private final Map<String, List<QuestionModelObject>> voteBallots;

  /**
   * Create a new {@link VotingCardModelObject} instance based on the given {@link VotingCardDeliveryType.VotingCard}.
   *
   * @param votingCard the voting card object extracted from an eCH-0228 XML file.
   *
   * @return the new {@link VotingCardModelObject} instance.
   */
  public static VotingCardModelObject get(VotingCardDeliveryType.VotingCard votingCard,
                                          List<ECH0159VoteInformation> voteInformationList) {
    return new VotingCardModelObject(
        getCodeByLabel(votingCard.getIndividualLogisticCode(), ECH0228CodeDesignation.CONFIRMATION),
        getCodeByLabel(votingCard.getIndividualLogisticCode(), ECH0228CodeDesignation.FINALIZATION),
        votingCard.getVotingPerson().getDeliveryAddress().getPerson(),
        getAddress(votingCard),
        getCodeByLabel(votingCard.getEVotingIndividualCodes().getContestCodes(),
                       ECH0228CodeDesignation.IDENTIFICATION),
        votingCard.getVotingPerson().getDomainOfInfluenceInfo().stream()
                  .map(VotingPersonType.DomainOfInfluenceInfo::getDomainOfInfluence)
                  .map(DomainOfInfluenceType::getDomainOfInfluenceName)
                  .collect(Collectors.joining(", ")),
        String.valueOf(votingCard.getVotingCardsequenceNumber()),
        getVoteBallots(votingCard, voteInformationList)
    );
  }

  private static String getCodeByLabel(List<NamedCodeType> codes, ECH0228CodeDesignation label) {
    return label.findCodeValue(codes).orElseThrow(
        () -> new IllegalStateException(String.format("Code with label [%s] not present.", label))
    );
  }

  private static String getAddress(VotingCardDeliveryType.VotingCard votingCard) {
    AddressInformationType address = votingCard.getVotingPerson().getDeliveryAddress().getAddressInformation();
    return Stream.of(address.getStreet(),
                     address.getHouseNumber(),
                     address.getTown(),
                     address.getSwissZipCode() != null ? String.format("%04d", address.getSwissZipCode()) : null,
                     address.getForeignZipCode(),
                     address.getCountry().getCountryNameShort())
                 .filter(addressLine -> !Strings.isNullOrEmpty(addressLine))
                 .collect(Collectors.joining("\n"));
  }

  private static Map<String, List<QuestionModelObject>> getVoteBallots(
      VotingCardDeliveryType.VotingCard votingCardType,
      List<ECH0159VoteInformation> voteInformationList) {

    return votingCardType.getEVotingIndividualCodes()
                         .getVote()
                         .stream()
                         .collect(
                             Collectors.toMap(VoteType::getVoteIdentification,
                                              voteType -> Stream.concat(
                                                  getVariantBallotQuestions(voteType, voteInformationList),
                                                  getStandardBallotQuestions(voteType, voteInformationList)
                                              ).collect(Collectors.toList()))
                         );

  }

  private static Stream<QuestionModelObject> getVariantBallotQuestions(
      VoteType voteType,
      List<ECH0159VoteInformation> voteInformationList) {
    return voteType.getBallot()
                   .stream()
                   .map(VoteType.Ballot::getVariantBallot)
                   .filter(Objects::nonNull)
                   .map(VoteType.Ballot.VariantBallot::getQuestion)
                   .flatMap(List::stream)
                   .map(question -> {
                     AnswerTypeEnum answerType =
                         findAnswerTypeByQuestionIdentifier(voteInformationList,
                                                            question.getQuestionIdentification());
                     return QuestionModelObject.get(question, answerType);
                   });
  }

  private static Stream<QuestionModelObject> getStandardBallotQuestions(
      VoteType voteType,
      List<ECH0159VoteInformation> voteInformationList) {
    return voteType.getBallot()
                   .stream()
                   .map(VoteType.Ballot::getStandardBallot)
                   .filter(Objects::nonNull)
                   .map(VoteType.Ballot.StandardBallot::getQuestion)
                   .map(question -> {
                     AnswerTypeEnum answerType =
                         findAnswerTypeByQuestionIdentifier(voteInformationList,
                                                            question.getQuestionIdentification());
                     return QuestionModelObject.get(question, answerType);
                   });
  }

  private static AnswerTypeEnum findAnswerTypeByQuestionIdentifier(
      List<ECH0159VoteInformation> voteInformationList, String questionIdentifier) {

    return voteInformationList.stream()
                              .filter(vote -> vote.hasQuestionIdentifier(questionIdentifier))
                              .map(ECH0159VoteInformation::getBallots)
                              .flatMap(List::stream)
                              .filter(ballot -> ballot.hasQuestionIdentifier(questionIdentifier))
                              .map(ballot -> ballot.getAnswerTypeCodeByQuestionIdentifier(questionIdentifier))
                              .map(AnswerTypeEnum::fromInt)
                              .findFirst()
                              .orElseThrow(() -> new IllegalStateException(
                                  String.format("No question found with identifier [%s]", questionIdentifier)));
  }

  private VotingCardModelObject(String confirmationCode,
                                String finalizationCode,
                                PersonMailAddressInfoType voter,
                                String address,
                                String identificationCode,
                                String domainOfInfluence,
                                String sequenceNumber,
                                Map<String, List<QuestionModelObject>> voteBallots) {
    this.confirmationCode = confirmationCode;
    this.finalizationCode = finalizationCode;
    this.voter = voter;
    this.address = address;
    this.identificationCode = identificationCode;
    this.domainOfInfluence = domainOfInfluence;
    this.sequenceNumber = sequenceNumber;
    this.voteBallots = voteBallots;
  }

  public String getConfirmationCode() {
    return confirmationCode;
  }

  public String getFinalizationCode() {
    return finalizationCode;
  }

  public PersonMailAddressInfoType getVoter() {
    return voter;
  }

  public String getAddress() {
    return address;
  }

  public String getIdentificationCode() {
    return identificationCode;
  }

  public String getDomainOfInfluence() {
    return domainOfInfluence;
  }

  public String getSequenceNumber() {
    return sequenceNumber;
  }

  public Map<String, List<QuestionModelObject>> getVoteBallots() {
    return voteBallots;
  }
}
