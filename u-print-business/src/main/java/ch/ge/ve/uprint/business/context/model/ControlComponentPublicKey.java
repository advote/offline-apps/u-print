/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.model;

import ch.ge.ve.protocol.model.IdentificationPublicKey;

/**
 * A value object that holds a control component's public key.
 */
public final class ControlComponentPublicKey {

  private final String                  id;
  private final IdentificationPublicKey publicKey;

  /**
   * Create a new control component public key for the given parameter.
   *
   * @param id        the key id.
   * @param publicKey the public key.
   */
  public ControlComponentPublicKey(String id, IdentificationPublicKey publicKey) {
    this.id = id;
    this.publicKey = publicKey;
  }

  public String getId() {
    return id;
  }

  public IdentificationPublicKey getPublicKey() {
    return publicKey;
  }
}
