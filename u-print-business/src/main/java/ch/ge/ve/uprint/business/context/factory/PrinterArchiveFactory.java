/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory;

import ch.ge.ve.filenamer.archive.PrinterArchiveReader;
import ch.ge.ve.javafx.business.progress.GradualProgressNotifier;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.uprint.business.context.exception.InvalidPrinterArchiveException;
import ch.ge.ve.uprint.business.context.model.PrinterArchive;
import com.google.common.base.Strings;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * A {@link PrinterArchive} provider.
 */
@Service
public class PrinterArchiveFactory {

  private static final Logger logger = LoggerFactory.getLogger(PrinterArchiveFactory.class);

  // allow a "dev mode" extraction to be triggered by a "extract.to.path" system property
  private static ExtractingStrategy selectExtractingStrategy(PrinterArchiveReader reader) {
    final String property = System.getProperty("extract.to.path");
    if (Strings.isNullOrEmpty(property)) {
      return new DefaultExtractingStrategy();
    } else {
      // only Locale-independent format
      @SuppressWarnings("squid:S1449")
      String qualifier = String.format("%1$s_%2$s_%3$tF-%3$tH%3$tM%3$tS",
                                       reader.getOperationName(), reader.getPrinterName(),
                                       reader.getCreationDate().orElseGet(LocalDateTime::now));
      return new DevModeExtractingStrategy(Paths.get(property), qualifier);
    }
  }

  private static long contentSize(Path source) throws IOException {
    try (ZipFile zip = new ZipFile(source.toFile())) {
      return zip.stream().mapToLong(ZipEntry::getSize).sum();
    }
  }

  /**
   * Create a new {@link PrinterArchive} from the given zipped source.
   *
   * @param source  the zipped printer archive file.
   * @param tracker the {@link ProgressTracker} strategy.
   *
   * @return a new instance of {@link PrinterArchive}.
   *
   * @throws InvalidPrinterArchiveException if there were duplicate or missing files or the ZIP was not formatted
   *                                        correctly.
   */
  public PrinterArchive readPrinterArchive(Path source, ProgressTracker tracker) {
    AtomicReference<PrinterArchive> result = new AtomicReference<>();
    PrinterArchiveReader.withArchive(source, reader -> result.set(createPrinterArchive(source, reader, tracker)));
    return result.get();
  }

  private PrinterArchive createPrinterArchive(Path source, PrinterArchiveReader reader, ProgressTracker tracker) {
    try {
      final ExtractingStrategy extractor = selectExtractingStrategy(reader);

      final long estimatedSize = contentSize(source);
      logger.debug("##  Zip size is {} bytes", estimatedSize);
      final GradualProgressNotifier gradualTracker = new GradualProgressNotifier(tracker, estimatedSize);

      final Path extractionPath = extractor.getExtractionFolder(source.getFileName().toString());
      logger.debug("Archive will be extracted to {}", extractionPath);

      final Holder holder = new Holder(gradualTracker, extractor, extractionPath);
      return new PrinterArchive(reader.getOperationName(), reader.getPrinterName(),
                                holder.copy(reader.getVotersReferenceSource(), "ech0045.xml"),
                                holder.copy(reader.getPublicParameters(), "public-params.json"),
                                holder.copy(reader.getElectionSetSource(), "election-set.json"),
                                holder.copy(reader.getOperationConfigurationSource(), "opconf.json"),
                                holder.copy(reader.getOperationReferenceSources()),
                                holder.copy(reader.getPrivateCredentialsSources()));
    } catch (Exception e) {
      throw new InvalidPrinterArchiveException("A problem occurred while parsing the printer archive", e);
    }
  }

  private static class Holder {
    private final GradualProgressNotifier tracker;
    private final ExtractingStrategy      strategy;
    private final Path                    extractionFolder;

    private Holder(GradualProgressNotifier tracker, ExtractingStrategy strategy, Path extractionFolder) {
      this.tracker = tracker;
      this.strategy = strategy;
      this.extractionFolder = extractionFolder;
    }

    private Path copy(InputStream inputStream, String name) throws IOException {
      final Path out = extractionFolder.resolve(strategy.resolveFilename(name));
      long size = Files.copy(inputStream, out);
      inputStream.close();

      logger.debug("##  .. extracted {} bytes for {}", size, out.getFileName());
      tracker.notifyProgress(size);
      return out;
    }

    private List<Path> copy(Map<String, InputStream> inputStreams) throws IOException {
      List<Path> paths = new ArrayList<>(inputStreams.size());
      for (Map.Entry<String, InputStream> entry : inputStreams.entrySet()) {
        paths.add(copy(entry.getValue(), entry.getKey()));
      }
      return paths;
    }
  }
}
