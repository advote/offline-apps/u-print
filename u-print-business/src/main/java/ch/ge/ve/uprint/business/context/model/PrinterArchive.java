/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.model;

import com.google.common.collect.ImmutableList;
import java.nio.file.Path;
import java.util.List;

/**
 * A value object that holds all the printer archive components.
 */
public class PrinterArchive {

  private final String operationName;
  private final String printerName;

  private final Path       ech0045;
  private final Path       publicParametersFile;
  private final Path       electionSetFile;
  private final Path       operationConfigurationFile;
  private final List<Path> operationReferenceFiles;
  private final List<Path> privateCredentials;

  /**
   * Create a new {@link PrinterArchive} instance.
   *
   * @param operationName              the name of the operation
   * @param printerName                the printer this archive is intended to
   * @param ech0045                    the ECH0045 file location.
   * @param publicParametersFile       the location of the public parameters file.
   * @param electionSetFile            the location of the election set file.
   * @param operationConfigurationFile the location of the operation configuration file.
   * @param operationReferenceFiles    the locations of the operation reference files.
   * @param privateCredentials         the locations of all the private credentials files.
   */
  public PrinterArchive(String operationName,
                        String printerName,
                        Path ech0045,
                        Path publicParametersFile,
                        Path electionSetFile,
                        Path operationConfigurationFile,
                        List<Path> operationReferenceFiles,
                        List<Path> privateCredentials) {
    this.operationName = operationName;
    this.printerName = printerName;
    this.ech0045 = ech0045;
    this.publicParametersFile = publicParametersFile;
    this.electionSetFile = electionSetFile;
    this.operationConfigurationFile = operationConfigurationFile;
    this.operationReferenceFiles = ImmutableList.copyOf(operationReferenceFiles);
    this.privateCredentials = ImmutableList.copyOf(privateCredentials);
  }

  public String getOperationName() {
    return operationName;
  }

  public String getPrinterName() {
    return printerName;
  }

  public Path getEch0045() {
    return ech0045;
  }

  public Path getPublicParametersFile() {
    return publicParametersFile;
  }

  public Path getElectionSetFile() {
    return electionSetFile;
  }

  public Path getOperationConfigurationFile() {
    return operationConfigurationFile;
  }

  /**
   * Return all the operation reference files (eCH-0157 and eCH-0159) contained in this printer archive. The returned
   * collection is immutable.
   *
   * @return the list of operation reference files.
   */
  public List<Path> getOperationReferenceFiles() {
    return operationReferenceFiles;
  }

  /**
   * Return all the private credentials file contained in this printer archive. The returned collection is immutable.
   *
   * @return the list of operation reference files.
   */
  public List<Path> getPrivateCredentials() {
    return privateCredentials;
  }
}
