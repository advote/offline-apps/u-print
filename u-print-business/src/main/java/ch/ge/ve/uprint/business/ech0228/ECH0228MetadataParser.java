/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0228;

import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingCardDeliveryType.ContestData;
import ch.ge.ve.uprint.business.ech0228.exception.ECH0228ParsingException;
import ch.ge.ve.uprint.business.printerfile.exception.PrinterFileGenerationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import org.codehaus.stax2.XMLInputFactory2;
import org.springframework.stereotype.Service;

/**
 * An object to parse an eCH-0228 and retrieve the metadata in its header.
 */
@Service
public class ECH0228MetadataParser {
  private final JAXBContext jaxbContext;

  private final XMLInputFactory xmlInputFactory = XMLInputFactory2.newInstance();

  /**
   * Create a new eCH-0228 metadata parser.
   */
  public ECH0228MetadataParser() {
    try {
      this.jaxbContext = JAXBContext.newInstance(ContestData.class);
    } catch (JAXBException e) {
      throw new PrinterFileGenerationException("Cannot initialize printer file generator", e);
    }
  }

  /**
   * Parse the header of the given eCH-0228 source.
   *
   * @param source the path to the eCH-0228 file.
   *
   * @return the parsed {@link PrinterOperationConfigurationVo}.
   */
  public PrinterOperationConfigurationVo parse(Path source) {
    ContestData contestData = null;

    try (InputStream in = new FileInputStream(source.toFile())) {
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      XMLStreamReader streamReader = xmlInputFactory.createXMLStreamReader(in);
      XMLEventReader eventReader = xmlInputFactory.createXMLEventReader(streamReader);

      while (eventReader.hasNext() && contestData == null) {
        XMLEvent event = eventReader.peek();

        if (isContestDataElement(event)) {
          contestData = unmarshaller.unmarshal(eventReader, ContestData.class).getValue();
        } else {
          eventReader.next();
        }
      }
    } catch (IOException | JAXBException | XMLStreamException e) {
      throw new ECH0228ParsingException(String.format("Error caught while parsing file [%s]", source), e);
    }

    if (contestData == null) {
      throw new ECH0228ParsingException(String.format("File [%s] has no contest data", source));
    }

    return toPrinterOperationConfigurationVo(contestData);
  }

  private PrinterOperationConfigurationVo toPrinterOperationConfigurationVo(ContestData contestData) {
    return new PrinterOperationConfigurationVo(
        contestData.getContestIdentification(),
        ECH0228CodeDesignation.VOTING_CARD_TITLE.findCodeValue(contestData.getEVotingContestCodes())
                                                .orElse(null)
    );
  }

  private boolean isContestDataElement(XMLEvent event) {
    return event.isStartElement() && ECH0228Utils.CONTEST_DATA.equals(event.asStartElement().getName().getLocalPart());
  }
}
