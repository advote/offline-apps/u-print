/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory;

import ch.ge.ve.javafx.business.json.JsonPathMapper;
import ch.ge.ve.javafx.business.json.exception.JsonParseException;
import ch.ge.ve.protocol.model.IdentificationPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.uprint.business.context.exception.InvalidPublicKeysFileException;
import ch.ge.ve.uprint.business.context.model.ControlComponentPublicKey;
import ch.ge.ve.uprint.business.context.model.ControlComponentsPublicKeysFile;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Preconditions;
import com.google.common.io.BaseEncoding;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A {@link ControlComponentPublicKey} provider.
 */
@Service
public class ControlComponentsPublicKeysFileFactory {

  private static final Pattern NAME_PATTERN = Pattern.compile("cc(\\d)");

  private final PublicParameters publicParameters;
  private final JsonPathMapper   jsonPathMapper;

  /**
   * Create a new factory instance with the given parameters.
   *
   * @param publicParameters the voting protocol public parameters.
   * @param jsonPathMapper   the json mapper.
   */
  @Autowired
  public ControlComponentsPublicKeysFileFactory(PublicParameters publicParameters, JsonPathMapper jsonPathMapper) {
    this.publicParameters = publicParameters;
    this.jsonPathMapper = jsonPathMapper;
  }

  /**
   * Read a {@link ControlComponentsPublicKeysFile} from the given source path.
   *
   * @param source the location of the public keys file.
   *
   * @return the read {@link ControlComponentsPublicKeysFile}.
   *
   * @throws InvalidPublicKeysFileException when an invalid public keys file is provided.
   */
  public ControlComponentsPublicKeysFile readControlComponentsPublicKeysFile(Path source) {
    Map<Integer, List<ControlComponentPublicKey>> publicKeys = readControlComponentsPublicKeys(source);
    if (publicKeys.size() != publicParameters.getS()) {
      throw new InvalidPublicKeysFileException(String.format("Expected %s public keys, got %s instead",
                                                             publicParameters.getS(), publicKeys.size()));
    }
    return new ControlComponentsPublicKeysFile(source, sha256Sum(source), publicKeys);
  }

  private Map<Integer, List<ControlComponentPublicKey>> readControlComponentsPublicKeys(Path source) {
    try {
      Map<String, List<PublicKey>> keys = jsonPathMapper.map(source, new TypeReference<>() {
      });

      return keys.entrySet().stream().collect(Collectors.toMap(e -> extractControlComponentIndex(e.getKey()),
                                                               e -> toControlComponentPublicKeys(e.getValue())));
    } catch (JsonParseException e) {
      throw new InvalidPublicKeysFileException("Can't read control components' public keys from " + source, e);
    }
  }

  private Integer extractControlComponentIndex(String name) {
    Matcher matcher = NAME_PATTERN.matcher(name);
    if (matcher.matches()) {
      try {
        Integer index = Integer.valueOf(matcher.group(1));
        Preconditions.checkArgument(index >= 0, "Negative control component index");
        return index;
      } catch (IllegalArgumentException e) {
        throw new InvalidPublicKeysFileException("Unknown control component: " + name, e);
      }
    }
    throw new InvalidPublicKeysFileException("Unknown control component: " + name);
  }

  private List<ControlComponentPublicKey> toControlComponentPublicKeys(List<PublicKey> publicKeys) {
    return publicKeys.stream()
                     .map(pk -> new ControlComponentPublicKey(pk.id, toIdentificationPublicKey(pk)))
                     .sorted(Comparator.comparing(ControlComponentPublicKey::getId))
                     .collect(Collectors.toList());
  }

  private IdentificationPublicKey toIdentificationPublicKey(PublicKey pk) {
    return new IdentificationPublicKey(pk.value, publicParameters.getIdentificationGroup());
  }

  private String sha256Sum(Path path) {
    try {
      return BaseEncoding.base16().encode(MessageDigest.getInstance("SHA-256").digest(Files.readAllBytes(path)));
    } catch (IOException | NoSuchAlgorithmException e) {
      throw new InvalidPublicKeysFileException("Can't read control components' public keys from " + path, e);
    }
  }

  private static final class PublicKey {
    private final String     id;
    private final BigInteger value;

    @JsonCreator
    public PublicKey(@JsonProperty("keyId") String id, @JsonProperty("publicKey") BigInteger value) {
      this.id = id;
      this.value = value;
    }
  }
}
