/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.api;

import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.uprint.business.context.model.Context;
import java.nio.file.Path;

/**
 * Provides access to the context of the U-Print application.
 */
public interface ContextService {
  /**
   * Get the context of the application.
   *
   * @return the {@link Context} of the application.
   */
  Context get();

  /**
   * Clear the printer archive import context of the application.
   */
  void clearPrinterArchiveContext();

  /**
   * Setup the context of the application for performing a printer archive import.
   *
   * @param printerPrivateKey      the printer's private key.
   * @param printerArchiveLocation the location of the printer archive.
   * @param printerFileDestination the output location of the printer file.
   * @param tracker                a {@link ProgressTracker} to keep track of the context setup.
   */
  void setupPrinterArchiveContext(IdentificationPrivateKey printerPrivateKey,
                                  Path printerArchiveLocation,
                                  Path printerFileDestination,
                                  ProgressTracker tracker);
}
