/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0228;

import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingCardDeliveryType;
import ch.ge.ve.uprint.business.ech0228.exception.ECH0228ParsingException;
import ch.ge.ve.uprint.business.solr.SolrCoreNavigator;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.model.VotingCardDocument;
import ch.ge.ve.uprint.business.util.Navigator;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.codehaus.stax2.XMLInputFactory2;

/**
 * A navigator of all the currently imported voting cards on the given solr client.
 */
public class VotingCardNavigator implements Navigator<VotingCardDeliveryType.VotingCard> {
  private final SolrCoreNavigator<VotingCardDocument> solrNavigator;
  private final Unmarshaller                          unmarshaller;

  private final XMLInputFactory xmlInputFactory = XMLInputFactory2.newInstance();

  /**
   * Create a new voting card {@link Navigator} instance.
   *
   * @param solrClient the {@link SolrClient} that contains the voting cards.
   */
  public VotingCardNavigator(SolrClient solrClient) {
    solrNavigator = new SolrCoreNavigator<>(solrClient,
                                            UprintSolrCore.VOTING_CARDS,
                                            new SolrQuery("*:*"),
                                            VotingCardDocument.class);

    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(VotingCardDeliveryType.VotingCard.class);
      unmarshaller = jaxbContext.createUnmarshaller();
    } catch (JAXBException e) {
      throw new ECH0228ParsingException("Cannot initialize voting card navigator", e);
    }
  }

  private VotingCardDeliveryType.VotingCard retrieveVotingCard(VotingCardDocument document) {
    try {
      VotingCardDeliveryType.VotingCard votingCard = null;

      XMLEventReader reader = xmlInputFactory.createXMLEventReader(
          new ByteArrayInputStream(document.getVotingCardXmlElement().getBytes(StandardCharsets.UTF_8))
      );

      while (reader.hasNext() && votingCard == null) {
        XMLEvent event = reader.peek();

        if (event.isStartElement() && "votingCard".equals(event.asStartElement().getName().getLocalPart())) {
          votingCard = unmarshaller.unmarshal(reader, VotingCardDeliveryType.VotingCard.class).getValue();
        } else {
          reader.next();
        }
      }

      reader.close();

      return votingCard;
    } catch (XMLStreamException | JAXBException e) {
      throw new ECH0228ParsingException(
          String.format("Cannot parse voting card for voter with id [%s]", document.getVoterId()), e
      );
    }
  }

  @Override
  public boolean hasNext() {
    return solrNavigator.hasNext();
  }

  @Override
  public VotingCardDeliveryType.VotingCard next() {
    return retrieveVotingCard(solrNavigator.next());
  }

  @Override
  public boolean hasPrevious() {
    return solrNavigator.hasPrevious();
  }

  @Override
  public VotingCardDeliveryType.VotingCard previous() {
    return retrieveVotingCard(solrNavigator.previous());
  }

  @Override
  public int currentIndex() {
    return solrNavigator.currentIndex();
  }

  @Override
  public VotingCardDeliveryType.VotingCard last() {
    return retrieveVotingCard(solrNavigator.last());
  }

  @Override
  public VotingCardDeliveryType.VotingCard first() {
    return retrieveVotingCard(solrNavigator.first());
  }

  @Override
  public Long size() {
    return solrNavigator.size();
  }

}
