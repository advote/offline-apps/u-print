/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.exception;

import ch.ge.ve.javafx.business.exception.ErrorCodeBasedException;

/**
 * Thrown when an invalid printer file is provided to the application.
 */
public class InvalidPrinterFileException extends ErrorCodeBasedException {
  /**
   * Create a new invalid printer file exception for the specified error code.
   *
   * @param errorCode the error code describing the nature of the exception.
   *
   * @see ErrorCodeBasedException#ErrorCodeBasedException(String, String)
   */
  public InvalidPrinterFileException(PrinterFileErrorCode errorCode) {
    super(errorCode.name(), errorCode.getMessage());
  }

  /**
   * Create a new invalid printer file exception for the specified error code and cause.
   *
   * @param errorCode the error code describing the nature of the exception.
   * @param cause     the cause of this exception.
   *
   * @see ErrorCodeBasedException#ErrorCodeBasedException(String, String, Throwable)
   */
  public InvalidPrinterFileException(PrinterFileErrorCode errorCode, Throwable cause) {
    super(errorCode.name(), errorCode.getMessage(), cause);
  }

  /**
   * Enumerates all the possible printer file importing errors.
   */
  public enum PrinterFileErrorCode {
    /**
     * The printer file cannot be parsed.
     */
    CANNOT_READ_FILE("A problem occurred while parsing the printer file"),
    /**
     * The printer file contains more than one eCH-0228.
     */
    DUPLICATE_ECH_0228("Duplicate eCH-0228"),
    /**
     * The printer file doesn't contain any eCH-0228 file.
     */
    MISSING_ECH_0228("Missing eCH-0228"),
    /**
     * The printer file doesn't contain any eCH-0157 or eCH-0159 reference file.
     */
    MISSING_OPERATION_REFERENCE("Missing operation reference files");

    private final String message;

    PrinterFileErrorCode(String message) {
      this.message = message;
    }

    /**
     * Get the default message of this error code.
     *
     * @return the default message of this error code.
     */
    public String getMessage() {
      return message;
    }
  }
}
