/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.solr;

import ch.ge.ve.uprint.business.solr.api.SolrService;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.exception.SolrRuntimeException;
import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A Solr service implementation that contains an {@link EmbeddedSolrServer}.
 */
@Service
public class EmbeddedSolrService implements SolrService {
  private static final Logger             logger = LoggerFactory.getLogger(EmbeddedSolrService.class);
  private final        EmbeddedSolrServer solrServer;

  /**
   * Create a new embedded solr service instance.
   *
   * @param solrServer the underlying {@link EmbeddedSolrServer} of this service.
   */
  @Autowired
  public EmbeddedSolrService(EmbeddedSolrServer solrServer) {
    this.solrServer = solrServer;
  }

  @Override
  public SolrClient getClient() {
    return solrServer;
  }

  @Override
  public void deleteAll() {
    logger.debug("Resetting all solr indexes");
    EnumSet.allOf(UprintSolrCore.class).forEach(this::delete);
  }

  @Override
  public void delete(UprintSolrCore... cores) {
    Arrays.stream(cores).forEach(core -> {
      logger.debug("Resetting Solr index {}", core);
      try {
        solrServer.deleteByQuery(core.getCoreName(), "*:*");
        solrServer.commit();
      } catch (SolrServerException | IOException e) {
        logger.error("Failed to reset Solr index {}", core, e);
        throw new SolrRuntimeException(String.format("Failed to reset %s index", core.getCoreName()), e);
      }
    });
  }

  @Override
  public void close() throws IOException {
    solrServer.close();
  }
}
