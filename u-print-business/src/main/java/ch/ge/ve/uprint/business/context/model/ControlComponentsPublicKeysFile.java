/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A value object representing a control components public keys file. It contains the path to the original file, the
 * digest of the file and all the public keys of each control component.
 */
public final class ControlComponentsPublicKeysFile {

  private final Path                                          path;
  private final String                                        sha256sum;
  private final Map<Integer, List<ControlComponentPublicKey>> publicKeys;

  /**
   * Create a new control component public key file with the given parameters
   *
   * @param path       the path to the control components public key file.
   * @param sha256sum  the file's digest.
   * @param publicKeys the public keys mapped by control component index.
   */
  public ControlComponentsPublicKeysFile(Path path,
                                         String sha256sum,
                                         Map<Integer, List<ControlComponentPublicKey>> publicKeys) {
    this.path = path;
    this.sha256sum = sha256sum;
    this.publicKeys = ImmutableMap.copyOf(
        publicKeys.entrySet().stream()
                  .collect(Collectors.toMap(Map.Entry::getKey, e -> ImmutableList.copyOf(e.getValue()))));
  }

  /**
   * Get the path to the control component public key file.
   *
   * @return the path to the control component public key file.
   */
  public Path getPath() {
    return path;
  }

  /**
   * Get the SHA-256 digest of the control component public key file (see {@link #getPath()}.
   *
   * @return the file digest.
   */
  public String getSha256sum() {
    return sha256sum;
  }

  /**
   * Get all the public keys mapped by control component index. The map returned by this method is immutable.
   *
   * @return the map of public keys by control component index.
   */
  public Map<Integer, List<ControlComponentPublicKey>> getPublicKeys() {
    return publicKeys;
  }
}
