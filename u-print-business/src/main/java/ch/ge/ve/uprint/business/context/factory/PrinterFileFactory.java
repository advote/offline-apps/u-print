/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory;

import ch.ge.ve.chvote.pactback.contract.printerarchive.PrinterOperationConfigurationVo;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.uprint.business.context.exception.InvalidPrinterFileException;
import ch.ge.ve.uprint.business.context.exception.InvalidPrinterFileException.PrinterFileErrorCode;
import ch.ge.ve.uprint.business.context.model.PrinterFile;
import ch.ge.ve.uprint.business.ech0228.ECH0228MetadataParser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A {@link PrinterFile} provider.
 */
@Service
public class PrinterFileFactory {

  private final ECH0228MetadataParser ech0228MetadataParser;

  /**
   * Create a new printer file factory.
   */
  @Autowired
  public PrinterFileFactory(ECH0228MetadataParser ech0228MetadataParser) {
    this.ech0228MetadataParser = ech0228MetadataParser;
  }

  /**
   * Create a new {@link PrinterFile} from the given zipped source.
   *
   * @param source  the zipped printer file.
   * @param tracker the {@link ProgressTracker} strategy.
   *
   * @return a new instance of {@link PrinterFile}.
   *
   * @throws InvalidPrinterFileException if there were duplicate or missing files or the ZIP was not formatted
   *                                     correctly.
   */
  public PrinterFile readPrinterFile(Path source, ProgressTracker tracker) {
    try (ZipInputStream zin = new ZipInputStream(Files.newInputStream(source))) {
      long sizeInBytes = Files.size(source);
      long bytesRead = 0;

      tracker.updateProgress(bytesRead, sizeInBytes);

      PrinterFileBuilder builder = new PrinterFileBuilder(
          ech0228MetadataParser, source, Files.createTempDirectory("u-print-printer-file")
      );

      ZipEntry entry = zin.getNextEntry();

      while (entry != null) {
        bytesRead = bytesRead + builder.addAndStore(entry.getName(), zin);
        entry = zin.getNextEntry();

        tracker.updateProgress(bytesRead, sizeInBytes);
      }

      tracker.updateProgress(1, 1);

      return builder.build();
    } catch (IOException e) {
      throw new InvalidPrinterFileException(PrinterFileErrorCode.CANNOT_READ_FILE, e);
    }
  }

  private static class PrinterFileBuilder {
    private static final Pattern ECH_0228_PATTERN = Pattern.compile("^eCH-0228_([\\w-]+)_([\\w-]+)_([\\w-]+).xml$");
    private static final Pattern OP_REF_PATTERN   = Pattern.compile("^(?!eCH[-]0228)(.*)?[.]xml");

    private final ECH0228MetadataParser ech0228MetadataParser;
    private final Path                  source;
    private final Path                  root;

    private Path                            ech0228File;
    private PrinterOperationConfigurationVo operationConfiguration;

    private List<Path> operationReferenceFile = new ArrayList<>();

    private PrinterFileBuilder(ECH0228MetadataParser ech0228MetadataParser, Path source, Path root) {
      this.ech0228MetadataParser = ech0228MetadataParser;
      this.source = source;
      this.root = root;
    }

    private long addAndStore(String name, InputStream in) throws IOException {
      Path outputLocation = root.resolve(name);
      long nbrOfBytes = Files.copy(in, outputLocation);

      if (ECH_0228_PATTERN.matcher(name).matches()) {
        setECH0228File(outputLocation);
      }

      if (OP_REF_PATTERN.matcher(name).matches()) {
        operationReferenceFile.add(outputLocation);
      }

      return nbrOfBytes;
    }

    private void setECH0228File(Path echPrinterFile) {
      if (this.ech0228File != null) {
        throw new InvalidPrinterFileException(PrinterFileErrorCode.DUPLICATE_ECH_0228);
      }

      this.operationConfiguration = ech0228MetadataParser.parse(echPrinterFile);
      this.ech0228File = echPrinterFile;
    }

    private <T> void checkNotNull(T obj, PrinterFileErrorCode errorCode) {
      if (obj == null) {
        throw new InvalidPrinterFileException(errorCode);
      }
    }

    private PrinterFile build() {
      checkNotNull(ech0228File, PrinterFileErrorCode.MISSING_ECH_0228);
      checkNotNull(operationReferenceFile, PrinterFileErrorCode.MISSING_OPERATION_REFERENCE);

      if (operationReferenceFile.isEmpty()) {
        throw new InvalidPrinterFileException(PrinterFileErrorCode.MISSING_OPERATION_REFERENCE);
      }

      return new PrinterFile(source, operationReferenceFile, ech0228File, operationConfiguration);
    }
  }
}
