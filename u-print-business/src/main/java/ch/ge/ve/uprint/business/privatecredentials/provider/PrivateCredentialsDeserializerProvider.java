/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.privatecredentials.provider;

import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.uprint.business.context.api.ContextService;
import ch.ge.ve.uprint.business.privatecredentials.PrivateCredentialsDeserializer;
import ch.ge.ve.uprint.business.solr.api.SolrService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * An {@link PrivateCredentialsDeserializer} provider. Call {@link #get(ProgressTracker)} to create a new {@link
 * PrivateCredentialsDeserializer} instance.
 */
@Service
public class PrivateCredentialsDeserializerProvider {

  private final ContextService            contextService;
  private final SolrService               solrService;
  private final ObjectMapper              objectMapper;
  private final ChannelSecurityAlgorithms channelSecurityAlgorithms;

  /**
   * Create a new provider instance.
   *
   * @param contextService the {@link ContextService}.
   * @param solrService    the {@link SolrService}.
   * @param objectMapper   the json mapper.
   */
  @Autowired
  public PrivateCredentialsDeserializerProvider(ContextService contextService, SolrService solrService,
                                                ObjectMapper objectMapper,
                                                ChannelSecurityAlgorithms channelSecurityAlgorithms) {
    this.contextService = contextService;
    this.solrService = solrService;
    this.objectMapper = objectMapper;
    this.channelSecurityAlgorithms = channelSecurityAlgorithms;
  }

  /**
   * Get a new instance of {@link PrivateCredentialsDeserializer}.
   *
   * @param tracker the {@link ProgressTracker} strategy of the new instance.
   *
   * @return the new instance of {@link PrivateCredentialsDeserializer}.
   */
  public PrivateCredentialsDeserializer get(ProgressTracker tracker) {
    return new PrivateCredentialsDeserializer(solrService.getClient(), objectMapper, channelSecurityAlgorithms,
                                              tracker, contextService.get());
  }
}
