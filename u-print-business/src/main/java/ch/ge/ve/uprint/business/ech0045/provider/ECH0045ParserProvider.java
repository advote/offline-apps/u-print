/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0045.provider;

import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.uprint.business.context.api.ContextService;
import ch.ge.ve.uprint.business.ech0045.ECH0045Parser;
import ch.ge.ve.uprint.business.solr.api.SolrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * An {@link ECH0045Parser} provider. Call {@link #get(ProgressTracker)} to create a new {@link ECH0045Parser}
 * instance.
 */
@Service
public class ECH0045ParserProvider {
  private final SolrService    solrService;
  private final ContextService contextService;

  /**
   * Create a new provider instance.
   *
   * @param solrService    the {@link SolrService}.
   * @param contextService the {@link ContextService}.
   */
  @Autowired
  public ECH0045ParserProvider(SolrService solrService,
                               ContextService contextService) {
    this.solrService = solrService;
    this.contextService = contextService;
  }

  /**
   * Get a new instance of {@link ECH0045Parser}.
   *
   * @param tracker the {@link ProgressTracker} strategy of the new instance.
   *
   * @return the new instance of {@link ECH0045Parser}.
   */
  public ECH0045Parser get(ProgressTracker tracker) {
    return new ECH0045Parser(solrService.getClient(), tracker, contextService.get().getPrinterArchive().getEch0045());
  }
}
