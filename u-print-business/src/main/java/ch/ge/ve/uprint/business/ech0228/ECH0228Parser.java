/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0228;

import ch.ge.ve.interfaces.ech.eCH0228.v1.VotingCardDeliveryType;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.uprint.business.ech0228.exception.ECH0228ParsingException;
import ch.ge.ve.uprint.business.solr.DocumentImporter;
import ch.ge.ve.uprint.business.solr.api.UprintSolrCore;
import ch.ge.ve.uprint.business.solr.model.ImportReport;
import ch.ge.ve.uprint.business.solr.model.VotingCardDocument;
import ch.ge.ve.uprint.business.xml.ECHUtils;
import com.google.common.base.Stopwatch;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import org.apache.solr.client.solrj.SolrClient;
import org.codehaus.stax2.XMLInputFactory2;
import org.codehaus.stax2.XMLOutputFactory2;

/**
 * An object to parse an eCH-0228 and to store the voting cards into the Solr index.
 */
public class ECH0228Parser extends DocumentImporter<VotingCardDocument> {
  private final Path        source;
  private final JAXBContext jaxbContext;

  private final XMLOutputFactory xmlOutputFactory = XMLOutputFactory2.newFactory();
  private final XMLInputFactory  xmlInputFactory  = XMLInputFactory2.newInstance();
  private final XMLEventFactory  eventFactory     = XMLEventFactory.newInstance();

  /**
   * Create a new eCH-0228 parser.
   *
   * @param solrClient      the solr client.
   * @param progressTracker a progress tracker strategy.
   * @param source          the eCH-0228 path.
   */
  public ECH0228Parser(SolrClient solrClient, ProgressTracker progressTracker, Path source) {
    super(solrClient, UprintSolrCore.VOTING_CARDS, progressTracker);

    this.source = source;

    try {
      this.jaxbContext = JAXBContext.newInstance(VotingCardDeliveryType.VotingCard.class);
    } catch (JAXBException e) {
      throw new ECH0228ParsingException("Cannot initialize eCH-0228 parser", e);
    }
  }

  @Override
  public ImportReport doImport() {
    Stopwatch stopwatch = Stopwatch.createStarted();

    try (InputStream in = new FileInputStream(source.toFile())) {
      long sizeInBytes = Files.size(source);

      XMLStreamReader streamReader = xmlInputFactory.createXMLStreamReader(in);
      XMLEventReader eventReader = xmlInputFactory.createXMLEventReader(streamReader);

      while (eventReader.hasNext()) {
        long bytesRead = streamReader.getLocation().getCharacterOffset();
        XMLEvent event = eventReader.peek();

        if (event.isStartElement() &&
            ECH0228Utils.VOTING_CARD.equals(event.asStartElement().getName().getLocalPart())) {
          this.next(toVotingCardDocument(eventReader), bytesRead, sizeInBytes);
        } else {
          eventReader.next();
        }
      }
    } catch (JAXBException | IOException | XMLStreamException | IllegalArgumentException e) {
      abort();
      throw new ECH0228ParsingException(String.format("Error caught while parsing file [%s]", source), e);
    }

    return new ImportReport(done(), stopwatch.stop().elapsed());
  }

  private VotingCardDocument toVotingCardDocument(XMLEventReader eventReader)
      throws JAXBException, XMLStreamException, UnsupportedEncodingException {

    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    XMLEventWriter writer = xmlOutputFactory.createXMLEventWriter(baos, StandardCharsets.UTF_8.name());

    writer.add(eventFactory.createStartDocument(StandardCharsets.UTF_8.name(), "1.0"));
    ECHUtils.setCommonPrefixes(writer);
    writer.setPrefix(ECH0228Utils.ECH_0228_P, ECH0228Utils.ECH_0228_NS);
    writer.add(eventFactory.createStartElement("", null, "root"));
    ECHUtils.writeCommonNamespaces(writer, eventFactory);
    writer.add(eventFactory.createNamespace(ECH0228Utils.ECH_0228_P, ECH0228Utils.ECH_0228_NS));

    JAXBElement<VotingCardDeliveryType.VotingCard> element =
        unmarshaller.unmarshal(eventReader, VotingCardDeliveryType.VotingCard.class);
    VotingCardDeliveryType.VotingCard votingCard = element.getValue();
    marshaller.marshal(element, writer);

    writer.add(eventFactory.createEndElement("", null, "root"));
    writer.add(eventFactory.createEndDocument());

    writer.flush();
    writer.close();

    return new VotingCardDocument(ECH0228Utils.retrievePersonId(votingCard),
                                  baos.toString(StandardCharsets.UTF_8.name()));
  }
}
