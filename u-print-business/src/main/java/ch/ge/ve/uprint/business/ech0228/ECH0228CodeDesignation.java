/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.ech0228;

import ch.ge.ve.interfaces.ech.eCH0228.v1.NamedCodeType;
import java.util.List;
import java.util.Optional;

/**
 * An enum with all known code designation of an ECH0228. Allows to easily create and retrieve data for {@link
 * NamedCodeType} elements.
 */
public enum ECH0228CodeDesignation {
  /**
   * The code designation of the voting card title.
   */
  VOTING_CARD_TITLE("VOTING_CARD_TITLE"),
  /**
   * The code designation of confirmation code in the voting card.
   */
  CONFIRMATION("CONFIRMATION"),
  /**
   * The code designation of finalization code in the voting card.
   */
  FINALIZATION("FINALIZATION"),
  /**
   * The code designation of the identification code in the voting card.
   */
  IDENTIFICATION("IDENTIFICATION"),
  /**
   * The code designation of the abstention code in the voting card.
   */
  EMPTY_LIST("ABSTENTION"),
  /**
   * The code designation of the blank code in the voting card.
   */
  BLANK_LIST("BLANK"),
  /**
   * The code designation of the verification code in the voting card.
   */
  VERIFICATION("VERIFICATION");

  private final String codeDesignation;

  ECH0228CodeDesignation(String codeDesignation) {
    this.codeDesignation = codeDesignation;
  }

  /**
   * Create new {@link NamedCodeType} instances with the code designation of this enum instance.
   *
   * @param code the code value (see {@link NamedCodeType#setCodeValue(String)}).
   *
   * @return the new {@link NamedCodeType} instance.
   */
  public NamedCodeType createNamedCodeType(String code) {
    NamedCodeType namedCodeType = new NamedCodeType();
    namedCodeType.setCodeDesignation(codeDesignation);
    namedCodeType.setCodeValue(code);
    return namedCodeType;
  }

  /**
   * Find the value identified by the code designation of this enum instance in the given list of codes.
   *
   * @param codes the list of {@link NamedCodeType}.
   *
   * @return the code's value.
   */
  public Optional<String> findCodeValue(List<NamedCodeType> codes) {
    return codes.stream()
                .filter(code -> codeDesignation.equals(code.getCodeDesignation()))
                .map(NamedCodeType::getCodeValue)
                .findFirst();
  }
}
