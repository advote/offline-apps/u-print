/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.model;

import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import java.nio.file.Path;

/**
 * Container to store the session-scoped objects of the application.
 */
public class Context {

  private ControlComponentsPublicKeysFile controlComponentsPublicKeysFile;
  private IdentificationPrivateKey        printerDecryptionKey;
  private PrinterArchive                  printerArchive;
  private PrinterFile                     printerFile;
  private Path                            printerFileDestination;

  public ControlComponentsPublicKeysFile getControlComponentsPublicKeysFile() {
    return controlComponentsPublicKeysFile;
  }

  public void setControlComponentsPublicKeysFile(ControlComponentsPublicKeysFile controlComponentsPublicKeysFile) {
    this.controlComponentsPublicKeysFile = controlComponentsPublicKeysFile;
  }

  public PrinterArchive getPrinterArchive() {
    return printerArchive;
  }

  public void setPrinterArchive(PrinterArchive printerArchive) {
    this.printerArchive = printerArchive;
  }

  public IdentificationPrivateKey getPrinterDecryptionKey() {
    return printerDecryptionKey;
  }

  public void setPrinterDecryptionKey(IdentificationPrivateKey printerDecryptionKey) {
    this.printerDecryptionKey = printerDecryptionKey;
  }

  public Path getPrinterFileDestination() {
    return printerFileDestination;
  }

  public void setPrinterFileDestination(Path printerFileDestination) {
    this.printerFileDestination = printerFileDestination;
  }

  public PrinterFile getPrinterFile() {
    return printerFile;
  }

  public void setPrinterFile(PrinterFile printerFile) {
    this.printerFile = printerFile;
  }
}
