/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.exception;

import ch.ge.ve.javafx.business.exception.ErrorCodeBasedException;

/**
 * Thrown when an invalid private key is provided to the application.
 */
public class InvalidPrivateKeyException extends ErrorCodeBasedException {
  /**
   * Create a new invalid private key exception for the specified error code.
   *
   * @param errorCode the error code describing the nature of the exception.
   *
   * @see ErrorCodeBasedException#ErrorCodeBasedException(String, String)
   */
  public InvalidPrivateKeyException(PrivateKeyErrorCode errorCode) {
    super(errorCode.name(), errorCode.getMessage());
  }

  /**
   * Create a new invalid private key exception for the specified error code and cause.
   *
   * @param errorCode the error code describing the nature of the exception.
   * @param cause     the cause of this exception.
   *
   * @see ErrorCodeBasedException#ErrorCodeBasedException(String, String, Throwable)
   */
  public InvalidPrivateKeyException(PrivateKeyErrorCode errorCode, Throwable cause) {
    super(errorCode.name(), errorCode.getMessage(), cause);
  }

  /**
   * Enumerates all the possible private key importing errors.
   */
  public enum PrivateKeyErrorCode {
    /**
     * The provided alias is not found in the private key file.
     */
    UNKNOWN_ALIAS("Private key not found for the provided alias"),
    /**
     * The private key file cannot be parsed.
     */
    INVALID_PRIVATE_KEY("Invalid private key");

    private final String message;

    PrivateKeyErrorCode(String message) {
      this.message = message;
    }

    /**
     * Get the default message of this error code.
     *
     * @return the default message of this error code.
     */
    public String getMessage() {
      return message;
    }
  }
}
