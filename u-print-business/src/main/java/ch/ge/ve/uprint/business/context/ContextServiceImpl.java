/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context;

import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.uprint.business.context.api.ContextService;
import ch.ge.ve.uprint.business.context.factory.PrinterArchiveFactory;
import ch.ge.ve.uprint.business.context.model.Context;
import java.nio.file.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The default implementation of {@link ContextService}.
 */
@Service
public class ContextServiceImpl implements ContextService {

  private final Context               context;
  private final PrinterArchiveFactory printerArchiveFactory;

  /**
   * Create an empty context.
   *
   * @param printerArchiveFactory the {@link PrinterArchiveFactory}
   */
  @Autowired
  public ContextServiceImpl(PrinterArchiveFactory printerArchiveFactory) {
    this.printerArchiveFactory = printerArchiveFactory;
    this.context = new Context();
  }

  @Override
  public void clearPrinterArchiveContext() {
    context.setPrinterFileDestination(null);
    context.setPrinterArchive(null);
    context.setPrinterDecryptionKey(null);
  }

  @Override
  public void setupPrinterArchiveContext(IdentificationPrivateKey printerPrivateKey,
                                         Path printerArchiveLocation,
                                         Path printerFileDestination,
                                         ProgressTracker tracker) {

    context.setPrinterDecryptionKey(printerPrivateKey);
    context.setPrinterFileDestination(printerFileDestination);
    context.setPrinterArchive(printerArchiveFactory.readPrinterArchive(printerArchiveLocation, tracker));
  }

  @Override
  public Context get() {
    return context;
  }
}
