/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Development mode extracting strategy: all files are extracted in the specified folder (so that the content can be
 * access for debugging) and with qualified entry names.
 */
class DevModeExtractingStrategy implements ExtractingStrategy {

  private final Path   rootFolder;
  private final String qualifier;

  /**
   * Create a new development extracting strategy with destination folder and the qualifier.
   *
   * @param rootFolder the destination folder for the extracted files.
   * @param qualifier  a qualifier that will be added at the end of the filename preceded by an underscore ('{code _}')
   *                   character.
   */
  DevModeExtractingStrategy(Path rootFolder, String qualifier) {
    this.rootFolder = rootFolder;
    this.qualifier = qualifier;
  }

  @Override
  public Path getExtractionFolder(String filename) throws IOException {
    final String baseName = removeExtension(filename);
    Path extractionFolder = rootFolder.resolve(baseName);

    if (Files.exists(extractionFolder)) {
      extractionFolder = firstAvailableFolderName(baseName);
    }
    return Files.createDirectories(extractionFolder);
  }

  private static String removeExtension(String filename) {
    final int index = filename.lastIndexOf('.');
    return index == -1 ? filename : filename.substring(0, index);
  }

  private Path firstAvailableFolderName(String baseName) {
    int counter = 1;
    Path candidate;
    do {
      candidate = rootFolder.resolve(String.format("%s(%d)", baseName, counter++));
    } while (Files.exists(candidate));
    return candidate;
  }

  @SuppressWarnings("squid:S1449") // Locale-independent toLowerCase call (only ".epf" is considered)
  @Override
  public String resolveFilename(String entryName) {
    if (entryName.toLowerCase().endsWith(".epf")) {
      return entryName;
    } else if (entryName.contains(".")) {
      final int index = entryName.lastIndexOf('.');
      return entryName.substring(0, index) + "_" + qualifier + "." + entryName.substring(index + 1);
    } else {
      return entryName + "_" + qualifier;
    }
  }
}
