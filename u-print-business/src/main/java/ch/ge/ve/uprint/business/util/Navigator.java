/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A specialised iterator that allows to traverse a collection in either direction, obtain the iterator's current
 * position, the number of elements that can be iterated and directly retrieve the head and the tail elements of the
 * collection.
 *
 * @param <T>
 */
public interface Navigator<T> extends Iterator<T> {
  /**
   * Return {@code true} if {@link #previous} would return an element rather than throwing an exception.
   *
   * @return {@code true} if {@link #previous} would return an element rather than throwing an exception.
   */
  boolean hasPrevious();

  /**
   * Return the previous element in the navigation.
   *
   * @return the previous element in the navigation.
   *
   * @throws NoSuchElementException if the navigation has no more elements.
   */
  T previous();

  /**
   * Return the current position of the navigation.
   *
   * @return the current position of the navigation.
   */
  int currentIndex();

  /**
   * Return the last element in the navigation and moves the cursor to its end.
   *
   * @return the last element in the navigation.
   */
  T last();

  /**
   * Return the first element in the navigation and moves the cursor to its beginning.
   *
   * @return the first element in the navigation.
   */
  T first();

  /**
   * Return the total number of elements in the navigation.
   *
   * @return the total number of elements in the navigation.
   */
  Long size();
}
