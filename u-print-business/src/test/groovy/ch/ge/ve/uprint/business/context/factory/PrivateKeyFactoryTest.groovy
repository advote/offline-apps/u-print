/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory

import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.uprint.business.CryptoTestConfig
import ch.ge.ve.uprint.business.TestResources
import ch.ge.ve.uprint.business.context.exception.InvalidPrivateKeyException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ContextConfiguration(classes = [CryptoTestConfig])
class PrivateKeyFactoryTest extends Specification {

  @Autowired
  PublicParameters publicParameters

  PrivateKeyFactory privateKeyFactory

  def setup() {
    privateKeyFactory = new PrivateKeyFactory(publicParameters)
  }

  def "should read key store"() {
    given:
    def source = TestResources.PRIVATE_KEY

    when:
    def privateKey = privateKeyFactory.readPrivateKey(source, "simulation password!".toCharArray())

    then:
    privateKey != null
  }

  def "should fail to open key store with an invalid password"() {
    given:
    def source = TestResources.PRIVATE_KEY

    when:
    privateKeyFactory.readPrivateKey(source, "wrong password!".toCharArray())

    then:
    thrown(InvalidPrivateKeyException)
  }
}
