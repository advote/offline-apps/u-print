/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.uprint.business.ArchiveHelper
import ch.ge.ve.uprint.business.TestResources
import ch.ge.ve.uprint.business.context.exception.InvalidPrinterFileException
import ch.ge.ve.uprint.business.ech0228.ECH0228MetadataParser
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class PrinterFileFactoryTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  ArchiveHelper helper = new ArchiveHelper(temporaryFolder)

  PrinterFileFactory printerFileFactory

  def setup() {
    printerFileFactory = new PrinterFileFactory(new ECH0228MetadataParser())
  }

  def "should create printer file from a valid zip"() {
    given:
    def source = TestResources.PRINTER_FILE

    when:
    def archive = printerFileFactory.readPrinterFile(source, Mock(ProgressTracker))

    then:
    archive.getSource() != null
    archive.getEch0228FilePath() != null
    archive.getOperationConfiguration() != null
    archive.getOperationReferenceFiles() != null
    archive.getOperationReferenceFiles().size() == 1
  }

  def "should fail to create printer archive if there is no operation reference file"() {
    given:
    def missingOpref = helper.repackage(TestResources.PRINTER_FILE) { String entry -> entry.startsWith("eCH-0159") }

    when:
    printerFileFactory.readPrinterFile(missingOpref, Mock(ProgressTracker))

    then:
    def ex = thrown InvalidPrinterFileException
    ex.message == 'Missing operation reference files'
  }

  def "should fail to create printer archive if there is no eCH-0228"() {
    given:
    def missingEch228 = helper.repackage(TestResources.PRINTER_FILE) { String entry -> entry.startsWith("eCH-0228") }

    when:
    printerFileFactory.readPrinterFile(missingEch228, Mock(ProgressTracker))

    then:
    def ex = thrown InvalidPrinterFileException
    ex.message == 'Missing eCH-0228'
  }
}
