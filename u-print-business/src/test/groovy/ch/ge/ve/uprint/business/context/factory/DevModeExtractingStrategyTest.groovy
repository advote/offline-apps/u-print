/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business.context.factory

import java.nio.file.Files
import java.nio.file.Path
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class DevModeExtractingStrategyTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  Path rootFolder

  void setup() {
    rootFolder = temporaryFolder.newFolder("extract-root").toPath()
  }

  def "ExtractionFolder should use the given filename and the configured root"() {
    given:
    def strategy = new DevModeExtractingStrategy(rootFolder, "test")

    expect:
    strategy.getExtractionFolder("my-archive.zip") == rootFolder.resolve("my-archive")
  }

  def "ExtractionFolder should use an incremental qualifier if the default path exists"() {
    given:
    def strategy = new DevModeExtractingStrategy(rootFolder, "test")
    Files.createDirectory(rootFolder.resolve("my-archive"))
    Files.createDirectory(rootFolder.resolve("my-archive(1)"))
    Files.createDirectory(rootFolder.resolve("my-archive(2)"))

    expect:
    strategy.getExtractionFolder("my-archive.zip") == rootFolder.resolve("my-archive(3)")
  }

  def "ResolveFilename should append the qualifier to an entry name without an extension"() {
    given:
    def qualifier = "this-is-a-qualifier"
    def strategy = new DevModeExtractingStrategy(rootFolder, qualifier)

    expect:
    strategy.resolveFilename("some-entry") == "some-entry_$qualifier"
  }

  def "ResolveFilename should insert the qualifier to an entry name with an extension"() {
    given:
    def qualifier = "this-is-a-qualifier"
    def strategy = new DevModeExtractingStrategy(rootFolder, qualifier)

    expect:
    strategy.resolveFilename("some-entry.xml") == "some-entry_${qualifier}.xml"
  }

  def "ResolveFilename should not modify an EPF entry"() {
    given:
    def qualifier = "this-is-a-qualifier"
    def strategy = new DevModeExtractingStrategy(rootFolder, qualifier)

    expect:
    strategy.resolveFilename("some-entry.epf") == "some-entry.epf"
  }
}
