/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business

import groovy.transform.CompileStatic
import java.nio.file.Path
import java.nio.file.Paths

/**
 * A helper class that lists all the tests resources.
 */
@CompileStatic
class TestResources {

  // Root folder
  static Path RESOURCES_ROOT = Paths.get(TestResources.getResource("/").toURI())

  // Control component keys
  static Path CONTROL_COMPONENT_KEYS_FOLDER = RESOURCES_ROOT.resolve("control-component-keys")
  static Path CONTROL_COMPONENT_KEYS = CONTROL_COMPONENT_KEYS_FOLDER.resolve("control-components-keys.json")
  static Path CONTROL_COMPONENT_KEYS_WITH_INVALID_CC = CONTROL_COMPONENT_KEYS_FOLDER.resolve("control-components-keys-with-invalid-cc.json")
  static Path CONTROL_COMPONENT_KEYS_WITH_INVALID_STRUCTURE = CONTROL_COMPONENT_KEYS_FOLDER.resolve("control-components-keys-with-invalid-structure.json")
  static Path CONTROL_COMPONENT_KEYS_WITH_MORE_KEY_THAN_EXPECTED = CONTROL_COMPONENT_KEYS_FOLDER.resolve("control-components-keys-with-more-keys-than-expected.json")

  // eCH-0045 resources
  static Path ECH0045_FOLDER = RESOURCES_ROOT.resolve("ech0045")
  static Path ECH_0045 = ECH0045_FOLDER.resolve("eCH-0045.xml")

  // eCH-0228 resources
  static Path ECH0228_FOLDER = RESOURCES_ROOT.resolve("ech0228")
  static Path ECH0228 = ECH0228_FOLDER.resolve("eCH-0228.xml")

  // Printer authority private keys
  static Path PRIVATE_KEY_FOLDER = RESOURCES_ROOT.resolve("pa-private-key")
  static Path PRIVATE_KEY = PRIVATE_KEY_FOLDER.resolve("private-key.pfx")

  // Printer archive resources
  static Path PRINTER_ARCHIVE_FOLDER = RESOURCES_ROOT.resolve("printer-archive")
  static Path PRINTER_ARCHIVE_VALID_MAJORITY_ELECTION = PRINTER_ARCHIVE_FOLDER.resolve("valid-majority-election.paf")
  static Path PRINTER_ARCHIVE_VALID_MULTIPLE_OP_REF = PRINTER_ARCHIVE_FOLDER.resolve("valid-multiple-op-ref.paf")
  static Path PRINTER_ARCHIVE_VALID_PROPORTIONAL_ELECTION = PRINTER_ARCHIVE_FOLDER.resolve("valid-proportional-election.paf")
  static Path PRINTER_ARCHIVE_VALID_STANDARD_BALLOT = PRINTER_ARCHIVE_FOLDER.resolve("valid-standard-ballot.paf")
  static Path PRINTER_ARCHIVE_VALID_VARIANT_BALLOT = PRINTER_ARCHIVE_FOLDER.resolve("valid-variant-ballot.paf")

  // Printer file resources
  static Path PRINTER_FILE_FOLDER = RESOURCES_ROOT.resolve("printer-file")
  static Path PRINTER_FILE = PRINTER_FILE_FOLDER.resolve("valid.zip")

}
