/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-print                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.uprint.business

import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer
import ch.ge.ve.javafx.business.json.JsonPathMapper
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms
import ch.ge.ve.protocol.core.model.AlgorithmsSpec
import ch.ge.ve.protocol.core.support.Hash
import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.protocol.support.PublicParametersFactory
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import java.security.Security
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer
import org.springframework.core.io.ClassPathResource

/**
 * A Spring context that initializes and holds cryptographic environment and variables for our tests.
 */
@Configuration
class CryptoTestConfig {

  @Bean
  static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer()
    YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean()
    yaml.setResources(new ClassPathResource("application-test.yaml"))
    configurer.setProperties(yaml.getObject())
    return configurer
  }

  @Bean
  InitializingBean securityProviderInitializer() {
    return {
      def provider = new BouncyCastleProvider()
      if (Security.getProvider(provider.getName()) == null) {
        Security.addProvider(provider)
      }
    }
  }

  @Bean
  ObjectMapper createObjectMapper() {
    def module = new SimpleModule()
    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer())
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())
    return new ObjectMapper().registerModule(module)
  }

  @Bean
  JsonPathMapper jsonPathMapper(ObjectMapper objectMapper) {
    return new JsonPathMapper(objectMapper)
  }

  @Bean
  PublicParameters publicParameters(@Value('${uprint.security-level}') int securityLevel,
                                    @Value('${uprint.nbrOfControlComponents}') int numberOfControlComponents) {
    PublicParametersFactory publicParametersFactory = PublicParametersFactory.forLevel(securityLevel)
    return publicParametersFactory.createPublicParameters(numberOfControlComponents)
  }

  @Bean
  Hash hash(PublicParameters publicParameters,
            @Value('${uprint.digest.algorithm}') String digestAlgorithm,
            @Value('${uprint.digest.provider}') String digestProvider) {
    return new Hash(digestAlgorithm, digestProvider, publicParameters.getSecurityParameters().getUpper_l())
  }

  @Bean
  RandomGenerator randomGenerator(@Value('${uprint.rng.algorithm}') String rngAlgorithm,
                                  @Value('${uprint.rng.provider}') String rngProvider) {
    return RandomGenerator.create(rngAlgorithm, rngProvider)
  }

  @Bean
  ChannelSecurityAlgorithms channelSecurityAlgorithms(PublicParameters publicParameters,
                                                      AlgorithmsSpec algorithms,
                                                      Hash hash,
                                                      RandomGenerator randomGenerator) {
    return new ChannelSecurityAlgorithms(algorithms, hash, publicParameters.getIdentificationGroup(), randomGenerator)
  }

  @Bean
  KeyEstablishmentAlgorithms keyEstablishmentAlgorithms(RandomGenerator randomGenerator) {
    return new KeyEstablishmentAlgorithms(randomGenerator)
  }

  @Bean
  AlgorithmsSpec algorithms(@Value('${uprint.keyspec-algorithm}') String keySpecAlgorithm,
                            @Value('${uprint.cipher.algorithm}') String cipherAlgorithm,
                            @Value('${uprint.cipher.provider}') String cipherProvider,
                            @Value('${uprint.digest.algorithm}') String digestAlgorithm,
                            @Value('${uprint.digest.provider}') String digestProvider,
                            @Value('${uprint.rng.algorithm}') String rngAlgorithm,
                            @Value('${uprint.rng.provider}') String rngProvider) {
    return new AlgorithmsSpec(keySpecAlgorithm, cipherProvider, cipherAlgorithm,
            digestProvider, digestAlgorithm, rngProvider, rngAlgorithm)
  }
}
